#ifndef QUEUE_H
#define QUEUE_H
#include <QString>
#define SZABL template <typename C>
SZABL
class queue
{
    C *wsk;
    int cursor_first, cursor_last, size_q;
public:
    queue(unsigned int size);
    ~queue();
    bool push(C e);
    C front();
    C back ();
    QString list();
    C zdejmij();

};

#endif // QUEUE_H