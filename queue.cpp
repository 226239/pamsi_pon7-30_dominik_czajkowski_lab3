#include "queue.h"
SZABL
queue <C> ::queue(unsigned int size)
{
    cursor_first = 0;
    cursor_last = 0;
    size_q = size;
    wsk = new C [size];
}

SZABL
bool queue <C> :: push(C e)
{
    if(cursor_last < size_q)
    {
        cursor_last ++;
        wsk[cursor_last] = e;
        return true;
    }
}

SZABL
C queue <C> :: front()
{
    return wsk[cursor_first];
}

SZABL
C queue <C> :: back()
{
    return wsk[cursor_last-1];
}

SZABL
QString queue <C> :: list()
{
    QString buf;
    if(cursor_first != cursor_last)
    {
    for(int i=cursor_first+1;i<cursor_last; i++)
        buf += " " + QString :: number(wsk[i]);
    }
    return buf;
}

SZABL
C queue <C> :: zdejmij()
{
    if (cursor_first != cursor_last && cursor_last != 0)
        return wsk[cursor_first++];
}

SZABL
queue <C> ::~queue()
{
    delete [] wsk;
}
